#include <sourcemod>

public Plugin myinfo =
{
	name = "+klook block"
};

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3])
{
    if ((buttons & IN_FORWARD || buttons & IN_BACK) && vel[0] == 0 && !(buttons & IN_FORWARD && buttons & IN_BACK))
    {
        vel[0] = buttons & IN_FORWARD ? 450.0 : -450.0;
        return Plugin_Changed;
    }
    return Plugin_Continue;
}